package org.example;

public class PettingZoo extends Zoo {
    private  String     foodType;
    @Override
    public boolean zooStatus() {
        return false;
    }

    public  void  setFoodType(String foodType)
    {
        this.foodType=foodType;
    }
    public  boolean ageControl()
    {
        if(PersonAge.age<4)
        {
            return false ;
        }else
        {
            return  true;
        }
    }
}
