package org.example;

public class SafariParks extends  Zoo{

    private  double sizeOfSafarPark;

    public void setSizeOfSafarPark(double sizeOfSafarPark) {
        this.sizeOfSafarPark = sizeOfSafarPark;
    }

    @Override
    public String toString() {
        return "SafariParks{" +
                "sizeOfSafarPark=" + sizeOfSafarPark +
                ", zooName='" + zooName + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

    public  boolean checkAnimal (DangerousAnimals dangerousAnimals, String checkAnimal)
    {
        for (int i = 0; i < dangerousAnimals.getDangerousAnimals().size(); i++) {
            if(dangerousAnimals.getDangerousAnimals().get(i)==checkAnimal)
            {
                return true;
            }

        }
        return false;
    }


    @Override
    public boolean zooStatus() {
        return true;
    }
}
