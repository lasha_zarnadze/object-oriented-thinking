package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //Employess
        Employees employees = new Employees();
        employees.setName("username: lasha");
        employees.setSurname("surname: zarnadze");
        employees.setEmoployeeID(1342);
        System.out.println("ID: "+employees.getEmoployeeID());
        System.out.println(employees.getName());
        System.out.println(employees.getSurname());
        System.out.println("--------------------------------------------");

        //Aquariums

        Aquariums myAquarium = new Aquariums(100); // Creating an Aquarium with 100 liters of water
        myAquarium.changeWaterVolume(150); // Changing water volume in the Aquarium
        System.out.println("--------------------------------------------");

        //DangerousAnimals

        // Creating an instance of DangerousAnimals class
        DangerousAnimals dangerousAnimals = new DangerousAnimals();
        // Creating a new list of dangerous animals
        List<String> newDangerousAnimals = new ArrayList<>(Arrays.asList("Tiger", "Crocodile", "Lion"));
        // Setting the new list of dangerous animals using the setDangerousAnimals() method
        dangerousAnimals.setDangerousAnimals(newDangerousAnimals);
        System.out.println(dangerousAnimals.getDangerousAnimals());
        System.out.println("--------------------------------------------");

        // SafariParks

        SafariParks safariParks = new SafariParks();
        safariParks.zooName="Africa Safare park";
        safariParks.location ="Africa";
        safariParks.setSizeOfSafarPark(3456.78);
        System.out.println( safariParks.toString());
        System.out.println("--------------------------------------------");
        System.out.println("Triger is dangerous animal? : "+safariParks.checkAnimal(dangerousAnimals,"Tiger"));
        System.out.println("Snake is dangerous animal? : " +safariParks.checkAnimal(dangerousAnimals,"snake"));


        //personAge

        PersonAge.age=2;


        //PettingZoo


        PettingZoo pettingZoo=new PettingZoo();
        pettingZoo.setFoodType("bread");
        System.out.println("--------------------------------------------");
        System.out.println( "Avalable age? :"+pettingZoo.ageControl());
    }

}