package org.example;

public class Water {
    public  double amountOfWater;
    public  String watertype;
    // Water class represents the water in the aquarium

    private int volume; // Volume of water in liters

    public Water(int volume) {
        this.volume = volume;
    }

    public void changeVolume(int newVolume) {
        this.volume = newVolume;
        System.out.println("Water volume changed to " + newVolume + " liters.");
    }

}
