package org.example;

public class Aquariums extends Zoo {
    private  int numberOfSeaAnimal;
    private  int aquariumName;
    private Water water; // Composition: Aquarium "has-a" Water
    public Aquariums(int initialWaterVolume) {
        this.water = new Water(initialWaterVolume); // Creating Water object within Aquarium constructor
    }
    @Override
    public boolean zooStatus() {
        return false;
    }
    public  String sellAquarium()
    {
        String aquariumName = String.valueOf(this.aquariumName);
        return "sell Aquarium: "+ aquariumName;
    }
    public  String buyAquarium(String aquarium)
    {
        return  "Buy aquarium: "+ aquarium;
    }



    public void changeWaterVolume(int newVolume) {
        water.changeVolume(newVolume); // Utilizing Water object's method within Aquarium class
    }

}
